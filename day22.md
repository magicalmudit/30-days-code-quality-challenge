It's time to turn an eye toward your database schema.

Please spend 20 minutes reading through yours carefully.

Some things you might look for:

- Inconsistent column names.
- Missing indices for columns you frequently query by.
- Missing unique indices to ensure uniqueness.
- Missing null constraints.
- Missing foreign key constraints.
- Maybe even install [bullet](https://github.com/flyerhzm/bullet) to
  detect N+1 queries in `activerecord` and `mongoid`.

