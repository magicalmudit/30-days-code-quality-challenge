# Extract a compound conditional.

## Exercise

Find a compound conditional in your code (just grep
for && and ||) and try extracting it into a well-named method. Try to
make the new method explain a little more about what the compound
conditional means at a higher level.

For more insight on how and why to extract compound
conditionals please watch first 5 minutes of [this video](https://www.youtube.com/watch?v=0rsilnpU1DU)

Below is an example of code before and after doing this exercise.

**Before**:

```
if user_created_account_today? && user_has_unconfirmed_email?
  prevent_user_from_posting
end
```

**After**:

```
if user_has_high_spam_risk?
  prevent_user_from_posting
end

private

def user_has_high_spam_risk?
  user_created_account_today? && user_has_unconfirmed_email?
end
```

## Why do this?

Always remember you are writing code for other humans. By extracting conditional code into well-named methods, 
you make life easier for the poor fellow who will the maintaining the code down the line.

