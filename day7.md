# Truncate a Overgrown Class

## Exercise
Sometimes, despite our best intentions, a few classes in our system get
large and unwieldy. This exercise is to take a small step toward slimming 
down one of those classes.

First, pick one class that looks like a good candidate and open it up. Next, 
scan the file to look for opportunities to extract a new object.

When I do this, I'm looking for groups of methods that "clump together"
in a related way.

Here are a few attributes that might identify "clumps" that may make
sense to extract together:

1. Several methods that take the same parameter.
2. Several methods that access the same instance data.
3. Several methods that include the same word in their name.

When you see several methods that possess some of the above attributes,
try extracting them into a new class and see if it feels like a
worthwhile improvement.

An important caveat: this refactoring might be tough to pull off 

Since you're working on a large class, you may find it has a lot of
coupling that resists extraction.

Alternatively, you might not be able to find a good candidate for
extraction.

In either case, here's a fallback task: improve SOMETHING about the
class, even if it's tiny. Here are a few ideas:

- Delete a stray comment.
- Improve a name.
- Make something private if it's only called internally.
- Improve the formatting/style of any ugly bits (got any trailing
  whitespace or inconsistent newlines?).
- Slim down a long method.
- Delete some unused code.

## Why do this?
1. Refactoring of these classes spares developers from needing to remember a 
   large number of attributes for a class. In other words, one can have a better
   mental model of the class.
2. In many cases, splitting classes into parts helps to avoid duplication of 
   code and functionality.