# Nuke TODO Comments

## Exercise 
Grep your codebase for TODO comments.

When you find one, I recommend you do one of the following: 

1. Is it out of date? Delete it.

2. Is it still relevant? Delete it and add it to whatever system you use
   to track work to be done, like GitHub Issues or Trello or Jira.

3. Are you unsure? Do a little research and/or track down the comment's
   author and get an answer. Then do 1 or 2 :).

## Why do this? 
Code is a lousy place to track todos. When a todo 
lives in your code, it can't be prioritized or scheduled, and tends to
get forgotten.

