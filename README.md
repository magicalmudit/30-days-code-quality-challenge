This repository consists of small exercises to improve the quality of
the codebase on which you work upon. These exercises are language
agnostic and can be completed by anyone who is passionate about code
quality.
This is inspired by [30 days code quality
challenge](https://www.codequalitychallenge.com/) by Ben Orenstein.
