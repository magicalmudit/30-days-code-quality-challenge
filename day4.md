# Excise Unused Code

## Exercise

Let's track down some code that's not actually in use and delete it. Just do it 
naturally and don't be attached to code you wrote! 

Dead code may include unused packages, libraries, APIs or tables.

Why to put extra overhead on compiler to do dead code elimination!

## Why do this?

1. For anyone new working on the project may waste their time understanding
   unused code.

2. People tend to become negligant with time and what happens over time is 
   that more and more old unused code is added to the codebase. This will increase 
   the confusion, potential misunderstanding and maintenance overhead.

3. Remove dead code can also increase the performance of your application.

